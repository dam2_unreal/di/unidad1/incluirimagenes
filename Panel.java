import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Panel extends JPanel {

	public Panel(){}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);	
	
		//Metodo que nos permite dibujar una imagen
		//drawImagen(Image img, int x, int y, ImageObserver io)
		//Se puede con Graphics o Graphics2D
		
		//3 Formas
		//Con Toolkit
		//Con ImageIO
		//Con ImageIcon convertiendola a Image
		
		//2 Formas de obtener la URL del fichero dependidnde de donde se encuentre
		//La ruta absoluta al fichero externo
		//Un fichero interno (Dentro del bin)
		
		//1 Forma: Toolkit
		//Fichero externo
			//Toolkit tk = Toolkit.getDefaultToolkit();
			//Image img = tk.getImage("src/Recursos/image1.jpg");
			//g.drawImage(img, 10, 10, this); //this se pone si usamos tk
		//Fichero interno
			//Image img2 = Toolkit.getDefaultToolkit().getImage(getClass().getResource("../Recursos/image1.jpg"));
			//g.drawImage(img2, 10, 10, this); //this se pone si usamos tk
		
		//2 Forma: ImageIO
		//Fichero externo
			//try{
			//	Image img = ImageIO.read(new File("src/Recursos/image1.jpg"));
			//	g.drawImage(img, 0, 0, this);
			//}catch(Exception e){}
		//Fichero Interno
			//try{
			//	Image img = ImageIO.read(getClass().getResource("../Recursos/image1.jpg"));
			//	g.drawImage(img,0,0,this);
			//}catch(Exception e){}
		
		//3 Forma ImageIcon
		//Fichero Externo
			//ImageIcon imgIc = new ImageIcon("src/Recursos/image1.jpg");
			//Image img = imgIc.getImage();
			//g.drawImage(img, 10, 10, this);
		//Fichero Interno
			//Image img = new ImageIcon(getClass().getResource("../Recursos/image1.jpg")).getImage();
		
	}	
}